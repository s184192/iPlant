package dtu.softwareproject.iplant;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.fragment.app.Fragment;


public class MeasuringFragment extends Fragment implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor lightSensor;
    private TextView mTextLight;

    private List<Float> list = new ArrayList<Float>();
    final Handler handler = new Handler();





    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mSensorManager = (SensorManager) this.getActivity().getSystemService(Activity.SENSOR_SERVICE);
        lightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    @Override
    public RelativeLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {


        RelativeLayout rl = (RelativeLayout) inflater.inflate(R.layout.measuring_fragment, container, false);
        mTextLight = rl.findViewById(R.id.textView_measuring);
        return rl;
    }



    public void onResume() {
        super.onResume();
        list.clear();
        if (lightSensor != null) {
            mSensorManager.registerListener(this, lightSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);
            handler.postDelayed(runnable, 5000);


        }
        else{
            mTextLight.setText("No light sensor detected.");
        }
    }

    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        handler.removeCallbacks(runnable);


    }




    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        list.add(sensorEvent.values[0]);




    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        return;
    }

    Runnable runnable = new Runnable(){
        @Override
        public void run(){
            Float[] array= list.toArray(new Float[list.size()]);
            Arrays.sort(array);
            float median = 0;

            // Should probably notify the user that no data was
            // measured instead of just setting the median to 0.
            if (array.length != 0) {
                if (array.length % 2 == 0)
                    median = (array[array.length / 2] + array[array.length / 2 - 1]) / 2;
                else median = array[array.length / 2];
            }

            ResultFragment fragment = new ResultFragment();
            Bundle args = new Bundle();
            args.putFloat("luxvalue", median);
            fragment.setArguments(args);
            ((MainActivity) getActivity()).changeFragment(fragment);
        }
    };
}