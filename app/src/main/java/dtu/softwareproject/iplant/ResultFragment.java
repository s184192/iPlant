package dtu.softwareproject.iplant;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ResultFragment extends Fragment implements SearchView.OnQueryTextListener {

    MainActivity ma;
    private float luxValue;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ma = (MainActivity) getActivity();
        luxValue = getArguments().getFloat("luxvalue");
    }

    @Override
    public RelativeLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        RelativeLayout ll = (RelativeLayout) inflater.inflate(R.layout.searching_fragment, container, false);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = ll.findViewById(R.id.search_view);
        // Assumes current activity is the searchable activity

        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getActivity().getComponentName());
        searchView.setSearchableInfo(searchableInfo);
        searchView.setIconifiedByDefault(false);


        ListView lv = ll.findViewById(R.id.plant_list_view);

        lv.setAdapter(ma.getPlantDatabase());
        ma.getPlantDatabase().filterLuxSearch("",luxValue);

        searchView.setOnQueryTextListener(this);

        return ll;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        ma.getPlantDatabase().filterLuxSearch(text,luxValue);
        ma.getPlantDatabase().notifyDataSetChanged();
        return false;
    }
}