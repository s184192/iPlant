package dtu.softwareproject.iplant;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.io.IOException;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;
    private CurvedBottomNavigationView navView = null;
    private PlantDatabase pd;
    private Fragment currentFragment;
    private FavouriteDatabase fdb;

    private boolean doubleBackToExitPressedOnce;


    private BottomNavigationViewEx.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.navigation_favorites:
                    if (currentFragment.getClass() != FavouriteFragment.class) {
                        navView.changeSelectedItem(menuItem.getItemId());
                        changeFragment(new FavouriteFragment());
                    }
                    return true;
                case R.id.navigation_home:
                    if (currentFragment.getClass() != HomeFragment.class) {
                        navView.changeSelectedItem(menuItem.getItemId());
                        changeFragment(new HomeFragment());
                    }
                    return true;
                case R.id.navigation_search:
                    if (currentFragment.getClass() != SearchFragment.class) {
                        navView.changeSelectedItem(menuItem.getItemId());
                        changeFragment(new SearchFragment());
                    }
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pd = new PlantDatabase(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide();

        currentFragment = new Fragment();
        setContentView(R.layout.activity_main);
        navView = findViewById(R.id.nav_view);
        // mTextMessage = findViewById(R.id.welcome_message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.navigation_home);
        try {
            fdb = new FavouriteDatabase(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Example of an callback function that must be passed as parameter, when interacting with the
    // PlantDatabase's getPlantData method.
    private boolean setText(HashMap<String, Object> plantData) {
        //mTextMessage = findViewById(R.id.welcome_message);
        mTextMessage.setText((String) plantData.get("name"));
        Log.i("MainActivity.setText()", "setText method called");
        return true;
    }

    public void changeFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Class fc = fragment.getClass();

        if (fc == MeasuringFragment.class) {
            ft.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
            ft.addToBackStack(null);
        } else if (fc == InfoFragment.class) {
            ft.addToBackStack(null);
        }
        ft.replace(R.id.container, fragment);
        ft.commit();
    }


    @Override
    public void onBackPressed() {

        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 0 && currentFragment.getClass() != HomeFragment.class) {
            changeFragment(new HomeFragment());
            navView.setSelectedItemId(R.id.navigation_home);
            navView.changeSelectedItem(R.id.navigation_home);
        } else if (currentFragment.getClass() == HomeFragment.class && doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else if (backStackEntryCount == 0) {

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        } else {
            super.onBackPressed();
        }
    }


    public PlantDatabase getPlantDatabase() {
        return pd;
    }

    public FavouriteDatabase getFavouriteDatabase() {
        return fdb;
    }
}