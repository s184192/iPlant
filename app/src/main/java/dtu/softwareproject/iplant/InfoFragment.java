package dtu.softwareproject.iplant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class InfoFragment extends Fragment {
    private String name;
    private String alt_name;
    private String max_light;
    private String min_light;
    private String water;
    private String description;

    public InfoFragment(String plantName, String plantAltName, String maxLight, String minLight, String water, String description) {
        this.name = plantName;
        this.alt_name = plantAltName;
        this.max_light = maxLight;
        this.min_light = minLight;
        this.water = water;
        this.description = description;

    }


    @Override
    public LinearLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.info_fragment, container, false);
        TextView tvN = ((TextView) ll.findViewById(R.id.Name));
        tvN.setText(name);
        TextView tvA = ((TextView) ll.findViewById(R.id.alt_name));
        tvA.setText("Alt-name: " + alt_name);
        TextView tvMax = ((TextView) ll.findViewById(R.id.max_light_level));
        tvMax.setText("Max light level: " + max_light);
        TextView tvMin = ((TextView) ll.findViewById(R.id.min_light_level));
        tvMin.setText("Min light level: " + min_light);
        TextView tvv = ((TextView) ll.findViewById(R.id.water_level));
        tvv.setText("Water: " + water);
        TextView tvD = ((TextView) ll.findViewById(R.id.description));
        tvD.setText("Description: " + description);

        return ll;
    }
}
