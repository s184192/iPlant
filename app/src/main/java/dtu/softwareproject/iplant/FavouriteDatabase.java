package dtu.softwareproject.iplant;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FavouriteDatabase extends BaseAdapter {

    private ArrayList<HashMap<String, Object>> mPlants;
    private SharedPreferences sharedPrefNames;
    private File jsonFile;
    private MainActivity ma;
    private SharedPreferences.Editor ed;

    public FavouriteDatabase(Activity context) throws IOException {
        this.sharedPrefNames = context.getPreferences(Context.MODE_PRIVATE);
        this.mPlants = new ArrayList<>();
        this.ma = (MainActivity) context;


    }

    public void addFavourite(HashMap<String, Object> plant) {
        Gson gson = new Gson();

        String name = plant.get("name").toString();
        String plantJson = gson.toJson(plant);

        if (!sharedPrefNames.contains(name)) {
            ed = sharedPrefNames.edit();
            ed.putString(plant.get("name").toString(), plantJson);
            ed.commit();

            populate();
        }
    }

    public void removeFavourite(HashMap<String, Object> plant){
        ed = sharedPrefNames.edit();
        ed.remove(plant.get("name").toString());
        ed.apply();
    }

    public void populate(){

        mPlants.clear();
        Gson gson = new Gson();
        Map map = sharedPrefNames.getAll();
        Set keySet = map.keySet();


        String[] plantkeyarray = new String[keySet.size()];
        Object[] keySetArray = keySet.toArray();
        for(int n = 0; n < keySet.size();n++){
            plantkeyarray[n] = (String)keySetArray[n];

        }
        Arrays.sort(plantkeyarray);

        for (int j = 0; j < keySet.size(); j++){

            String jsonstr = (String) map.get(plantkeyarray[j]);
            HashMap<String, Object> plant = gson.fromJson(jsonstr, HashMap.class);

            mPlants.add(plant);


        }
    }

    public boolean contains(HashMap<String, Object> plant) {
        String name = plant.get("name").toString();
        return sharedPrefNames.contains(name);
    }


    // ADAPTER METHODS BELOW //
    @Override
    public int getCount() {
        ed = sharedPrefNames.edit();
        return sharedPrefNames.getAll().size();
    }

    @Override
    public HashMap<String, Object> getItem(int i) {
        return mPlants.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        } else {
            result = convertView;
        }

        HashMap<String, Object> item = getItem(position);

        TextView tv = ((TextView) result.findViewById(R.id.textView1));
        tv.setText((CharSequence) item.get("name"));


        tv.setText((CharSequence) item.get("name"));
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView tv = (TextView) view;
                String text = (String) tv.getText();

                HashMap<String, Object> value = getPlant(text);

                String plantName = (String) value.get("name");
                String plantAltName = (String) value.get("alt-name");
                String maxLight = (String) value.get("max-light-level");
                String minLight = (String) value.get("min-light-level");
                String water = (String) value.get("water");
                String description = (String) value.get("description");

                InfoFragment infof = new InfoFragment(plantName, plantAltName, maxLight, minLight, water, description);

                ma.changeFragment(infof);
            }
        });

        CheckBox cb = (CheckBox) result.findViewById(R.id.checkBox2);
        cb.setChecked(ma.getFavouriteDatabase().contains(item));


        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()) {
                    addFavourite(item);
                } else {
                    removeFavourite(item);
                }
                populate();
                notifyDataSetChanged();
            }
        });
        return result;
    }

    private HashMap<String, Object> getPlant(String plantName) {
        for (HashMap<String, Object> plant : mPlants) {
            if (plant.get("name").equals(plantName)) {
                return plant;
            }
        }
        return null;
    }
}
