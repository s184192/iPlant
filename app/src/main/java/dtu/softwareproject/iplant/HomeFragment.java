package dtu.softwareproject.iplant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment {

    @Override
    public LinearLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.homescreen_fragment, container, false);

        // This button is the big leaf button on the home screen
        ImageButton button = linearLayout.findViewById(R.id.measure_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).changeFragment(new MeasuringFragment());
                // This is where the light-level measuring should be handled from I think

            }
        });
        return linearLayout;
    }
}
