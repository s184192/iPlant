package dtu.softwareproject.iplant;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

@SuppressWarnings("FieldCanBeLocal")
class CurvedBottomNavigationView extends BottomNavigationViewEx {
    private static final String TAG = "CurvedBottomNavigationView";

    private Path mPath;
    private Paint mPaint;

    // The last and current selected positions. Used to get
    // a smooth moving curve from one selected item to another.
    private int previousIndexPosition = 2;
    private int currentIndexPosition;

// region Bezier curve variables
// ---------------------------------------------------------------------------
    // Width and height of the navBar
    private int mNavBarWidth;
    private int mNavBarHeight;

    // Animation related time variables (in milliseconds)
    private final static int ANIMATION_TIME = 300; // Duration of the entire animation.
    private static final int ICON_ANIMATION_TIME = 200; // Duration of icon animation.
    private static final int ICON_ANIMATION_TIME_OFFSET = ANIMATION_TIME - ICON_ANIMATION_TIME;

    // Various curve circle radius related measurements
    private static int CCR = 0; // CURVE_CIRCLE_RADIUS
    private static int curveWidthModifier = 0;  // 7/3 * CCR  |  CCR*2 + CCR/3
    private static int curveHeightModifier = 0; // 5/4 * CCR  |  CCR + CCR/4

    // A multiplier to get the curve to align with the navBar icons
    private final static double idxFactor1 = 1.0/6.0;
    private final static double idxFactor2 = 1.0/2.0;
    private final static double idxFactor3 = 1.0-idxFactor1;

    // The coordinates of the first curve
    private Point mFirstCurveStartPoint = new Point();
    private Point mFirstCurveEndPoint = new Point();
    private Point mFirstCurveControlPoint1 = new Point();
    private Point mFirstCurveControlPoint2 = new Point();

    // The coordinates of the second curve
    private Point mSecondCurveStartPoint = new Point();
    private Point mSecondCurveEndPoint = new Point();
    private Point mSecondCurveControlPoint1 = new Point();
    private Point mSecondCurveControlPoint2 = new Point();

    private static boolean selectionAnimationIsRunning = false;
    private long timeAnimationStart = 0;

    private static int curveMidPointOrigin = 0;
    private static int curveMidPointDestination = 0;
    private static int curveMidPointCurrent = 0;
    private static double curveMidPointTick = 0;

    private static int iconSizeDefault;
    private static int iconSizeSelected;
    private static double iconSizeTickDelta;

    private static int selectedIconTranslationOffset;
    private static double iconTranslationTickDelta;

    private static boolean nextIconAnimationIsDone = false;
    private static boolean prevIconAnimationIsDone = false;
    private long elapsedTime = 0;

// ---------------------------------------------------------------------------
// endregion Bezier curve variables


    public CurvedBottomNavigationView(Context context) {
        super(context);
        init();
    }

    public CurvedBottomNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CurvedBottomNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(Color.parseColor("#A00C380A"));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        updatedSizeRelatedVariables();
        startSelectionAnimation();
    }

    private void updatedSizeRelatedVariables() {
        // Width and height of navigation bar
        mNavBarWidth = getWidth();
        mNavBarHeight = getHeight();

        // Icon offset variables
        selectedIconTranslationOffset = -mNavBarHeight/10;
        iconTranslationTickDelta = (double) selectedIconTranslationOffset / ICON_ANIMATION_TIME;

        // Curve radius variables
        CCR = (int)(0.65 * mNavBarHeight);
        curveWidthModifier  = (int)(2.1 * CCR);
        curveHeightModifier = (int)(1.3 * CCR);
		
		// Icon sizes
        iconSizeDefault  = (int)(mNavBarHeight/8);
        iconSizeSelected = (int)(mNavBarHeight/6);
		iconSizeTickDelta = ((double)iconSizeSelected - iconSizeDefault) / ICON_ANIMATION_TIME;
    }

    public void changeSelectedItem(int itemID) {
        if (selectionAnimationIsRunning)
            instantCompleteAnimation();
        switch (itemID) {
            case R.id.navigation_favorites:
                currentIndexPosition = 1;
                break;
            case R.id.navigation_search:
                currentIndexPosition = 3;
                break;
            default:
                currentIndexPosition = 2;
        }
        if (currentIndexPosition != previousIndexPosition)
            startSelectionAnimation();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        doSelectionAnimationIfNecessary();
        if (selectionAnimationIsRunning)
            updateIconTransformations();
        selectionAnimationIsRunning = selectionAnimationIsRunning && elapsedTime < ANIMATION_TIME;
        canvas.drawPath(mPath, mPaint);
    }

    private void startSelectionAnimation() {
        // Initiates an "animation" for navBar selection.
        selectionAnimationIsRunning = true;

        // Saved time where animation began, used for interpolating the curve.
        timeAnimationStart = System.currentTimeMillis();

        prevIconAnimationIsDone = false;
        nextIconAnimationIsDone = false;

        // Curve points
        curveMidPointOrigin = getCurveMidPointForIndex(previousIndexPosition);
        curveMidPointDestination = getCurveMidPointForIndex(currentIndexPosition);
        curveMidPointTick = (double)(curveMidPointDestination - curveMidPointOrigin) / ANIMATION_TIME;

        doSelectionAnimationIfNecessary();
    }

    private void doSelectionAnimationIfNecessary() {
        if (!selectionAnimationIsRunning) return;

        elapsedTime = System.currentTimeMillis() - timeAnimationStart;
        if (elapsedTime > ANIMATION_TIME)
            elapsedTime = ANIMATION_TIME;

        tickCurveAnimation(elapsedTime);

        if (elapsedTime >= ANIMATION_TIME)
            previousIndexPosition = currentIndexPosition;

        postInvalidateDelayed(4);
    }

    private void instantCompleteAnimation() {
        this.setIconSizeAt(currentIndexPosition-1, iconSizeSelected, iconSizeSelected);
        this.setIconSizeAt(previousIndexPosition-1, iconSizeDefault, iconSizeDefault);

        this.getIconAt(currentIndexPosition-1).setTranslationY(selectedIconTranslationOffset);
        this.getIconAt(previousIndexPosition-1).setTranslationY(0);

        tickCurveAnimation(ANIMATION_TIME);
        previousIndexPosition= currentIndexPosition;
        selectionAnimationIsRunning = false;
    }


    private void updateIconTransformations() {
        int prev = previousIndexPosition -1;
        int next = currentIndexPosition-1;

        // Icon resize of previously selected item.
        if (!prevIconAnimationIsDone) {
            int prevItemAnimationTime = (int)(elapsedTime >= ICON_ANIMATION_TIME ? ICON_ANIMATION_TIME : elapsedTime);
            int tempIconSize = iconSizeSelected - (int)(prevItemAnimationTime * iconSizeTickDelta);
            int tempTranslationOffset = selectedIconTranslationOffset - (int)(prevItemAnimationTime * iconTranslationTickDelta);

            this.setIconSizeAt(prev, tempIconSize, tempIconSize);
            this.getIconAt(prev).setTranslationY(tempTranslationOffset);

            if (prevItemAnimationTime >= ICON_ANIMATION_TIME) {
                this.setIconSizeAt(prev, iconSizeDefault, iconSizeDefault);
                prevIconAnimationIsDone = true;
            }
        }

        // Icon resize of newly selected item.
        if (!nextIconAnimationIsDone) {
            int nextItemAnimationTime = (int)(elapsedTime >= ANIMATION_TIME ? ICON_ANIMATION_TIME : elapsedTime - ICON_ANIMATION_TIME_OFFSET);

            if (nextItemAnimationTime >= 0) {
                int tempIconSize = iconSizeDefault + (int)(nextItemAnimationTime * iconSizeTickDelta);
                int tempTranslationOffset = (int)(nextItemAnimationTime * iconTranslationTickDelta);
                this.setIconSizeAt(next, tempIconSize, tempIconSize);
                this.getIconAt(next).setTranslationY(tempTranslationOffset);
            }

            if (nextItemAnimationTime >= ICON_ANIMATION_TIME) {
                this.setIconSizeAt(next, iconSizeSelected, iconSizeSelected);
                nextIconAnimationIsDone = true;
            }
        }
    }


// region Bezier curve stuff
    //======================================================//
    //  ---  ---  ---   Bezier Curve Magic   ---  ---  ---  //
    //======================================================//

    private void tickCurveAnimation(long elapsedTime) {
        curveMidPointCurrent = curveMidPointOrigin + (int)(curveMidPointTick*elapsedTime);

        // the coordinates (x,y) of the start point before curve
        mFirstCurveStartPoint.set(
                curveMidPointCurrent - curveWidthModifier,
                0);

        // the coordinates (x,y) of the end point after curve
        mFirstCurveEndPoint.set(
                curveMidPointCurrent,
                curveHeightModifier);

        // same thing for the second curve
        mSecondCurveStartPoint = mFirstCurveEndPoint;
        mSecondCurveEndPoint.set(
                curveMidPointCurrent + curveWidthModifier,
                0);

        // the coordinates (x,y)  of the 1st control point on a cubic curve
        mFirstCurveControlPoint1.set(
                mFirstCurveStartPoint.x + curveHeightModifier,
                mFirstCurveStartPoint.y);

        // the coordinates (x,y)  of the 2nd control point on a cubic curve
        mFirstCurveControlPoint2.set(
                mFirstCurveEndPoint.x - CCR,
                mFirstCurveEndPoint.y);

        mSecondCurveControlPoint1.set(
                mSecondCurveStartPoint.x + CCR,
                mSecondCurveStartPoint.y);
        mSecondCurveControlPoint2.set(
                mSecondCurveEndPoint.x - curveHeightModifier,
                mSecondCurveEndPoint.y);

        updatePath();
    }

    private void updatePath() {
        mPath.reset();
        mPath.lineTo(
                mFirstCurveStartPoint.x,
                mFirstCurveStartPoint.y);

        mPath.cubicTo(
                mFirstCurveControlPoint1.x, mFirstCurveControlPoint1.y,
                mFirstCurveControlPoint2.x, mFirstCurveControlPoint2.y,
                mFirstCurveEndPoint.x, mFirstCurveEndPoint.y);

        mPath.cubicTo(
                mSecondCurveControlPoint1.x, mSecondCurveControlPoint1.y,
                mSecondCurveControlPoint2.x, mSecondCurveControlPoint2.y,
                mSecondCurveEndPoint.x, mSecondCurveEndPoint.y);

        mPath.lineTo(
                mNavBarWidth,
                0);
        mPath.lineTo(
                mNavBarWidth,
                mNavBarHeight);
        mPath.lineTo(
                0,
                mNavBarHeight);
        mPath.close();
    }

    private int getCurveMidPointForIndex (int idx) {
        switch (idx) {
            case 1:
                return (int)(mNavBarWidth * idxFactor1);
            case 3:
                return (int)(mNavBarWidth * idxFactor3);
            default:
                return (int)(mNavBarWidth * idxFactor2);
        }
    }
    // endregion Bezier curve stuff
}