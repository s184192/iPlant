package dtu.softwareproject.iplant;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;

public class PlantDatabase extends BaseAdapter {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Map<String, HashMap<String, Object>> plantsDataMap = new HashMap<>();
    private final ArrayList<HashMap<String, Object>> mData;
    private ArrayList<HashMap<String, Object>> mSearched;
    private MainActivity ma;


    public PlantDatabase(MainActivity act) {
        mData = new ArrayList<>();
        mSearched = new ArrayList<>();
        saveInstanceOfFirestore();
        this.ma = act;
    }

    private void saveInstanceOfFirestore() {
        db.collection("houseplants")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (document != null) {
                                    plantsDataMap.put(document.getId(), (HashMap<String, Object>) document.getData());
                                    Log.i(document.getId(), document.getData().toString());
                                }
                            }
                            mData.addAll(plantsDataMap.values());
                            Log.i("saveInstanceOfFirestore", "Firestore saved.");
                        } else {
                            Log.d("saveInstanceOfFirestore", "Error getting documents from firestore");
                        }
                    }
                });
    }

    @Override
    public int getCount() {
        return mSearched.size();
    }

    @Override
    public HashMap<String, Object> getItem(int i) {
        return mSearched.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View result;

        if (convertView == null) {
            result = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        } else {
            result = convertView;
        }

        HashMap<String, Object> item = getItem(position);

        TextView tv = (TextView) result.findViewById(R.id.textView1);
        tv.setText((CharSequence) item.get("name"));

        CheckBox cb = (CheckBox) result.findViewById(R.id.checkBox2);
        cb.setChecked(ma.getFavouriteDatabase().contains(item));

        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tv = (TextView) view;
                String text = (String) tv.getText();
                Log.i("onclick test", text);
                HashMap<String, Object> value = getPlant(text);

                String plantName = (String) value.get("name");
                String plantAltName = (String) value.get("alt-name");
                String maxLight = (String) value.get("max-light-level");
                String minLight = (String) value.get("min-light-level");
                String water = (String) value.get("water");
                String description = (String) value.get("description");

                InfoFragment infof = new InfoFragment(plantName, plantAltName, maxLight, minLight, water, description);

                ma.changeFragment(infof);
            }
        });

        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()) {
                    ma.getFavouriteDatabase().addFavourite(item);
                } else {
                    ma.getFavouriteDatabase().removeFavourite(item);
                }
                notifyDataSetChanged();
            }
        });

        return result;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mSearched.clear();
        if (charText.length() == 0) {
            mSearched.addAll(mData);
        } else {
            for (Map.Entry<String, HashMap<String, Object>> plants : plantsDataMap.entrySet()) {
                HashMap<String, Object> value = plants.getValue();

                String plantName = (String) value.get("name");

                if (plantName.toLowerCase().contains(charText)) {
                    mSearched.add(value);

                }
            }
        }
        notifyDataSetChanged();
    }


    public void filterLuxSearch(String charText, float lux) {
        charText = charText.toLowerCase(Locale.getDefault());
        mSearched.clear();
        for (Map.Entry<String, HashMap<String, Object>> plants : plantsDataMap.entrySet()) {
            HashMap<String, Object> value = plants.getValue();

            String plantName = (String) value.get("name");
            float lowerLux = Float.parseFloat((String) value.get("min-light-level"));
            float upperLux = Float.parseFloat((String) value.get("max-light-level"));


            if (plantName.toLowerCase().contains(charText) && lowerLux <= lux && upperLux >= lux) {
                mSearched.add(value);
            }
        }


        notifyDataSetChanged();
    }

    public HashMap<String, Object> getPlant(String name) {
        return plantsDataMap.get(name);
    }
}
