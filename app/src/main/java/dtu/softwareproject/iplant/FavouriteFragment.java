package dtu.softwareproject.iplant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

public class FavouriteFragment extends Fragment {
    MainActivity ma;

    @Override
    public RelativeLayout onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        RelativeLayout ll = (RelativeLayout) inflater.inflate(R.layout.favourite_fragment, container, false);

        ListView lv = ll.findViewById(R.id.plant_list_view_fav);

        ma = (MainActivity) getActivity();
        ma.getFavouriteDatabase().populate();

        lv.setAdapter(ma.getFavouriteDatabase());

        return ll;
    }
    //get favourite database (to populate mPlants)
}
